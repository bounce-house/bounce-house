﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballBounce : MonoBehaviour
{
    public Transform cameraDir;
    public float upForce;
    public float forwardForce;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        Debug.DrawRay(cameraDir.position, cameraDir.forward * 10f);
        RaycastHit hit;
        if(Physics.Raycast(cameraDir.position,cameraDir.forward,out hit))
        {
            if (hit.transform.gameObject.tag == "Ball")
            {
                print("ball!");

                if (Input.GetMouseButtonDown(0))
                {
                    hit.transform.GetComponent<Rigidbody>().AddForce(cameraDir.forward * forwardForce);
                    hit.transform.GetComponent<Rigidbody>().AddForce(cameraDir.up * upForce);

                }
            }
        }
    }
}
