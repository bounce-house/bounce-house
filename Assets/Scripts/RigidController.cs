﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class RigidController : MonoBehaviour
{
		public float Speed = 5f;
		public float JumpHeight = 2f;
		public float GroundDistance = 0.2f;
		public float DashDistance = 5f;
		public LayerMask Ground;

		private Rigidbody body;
		private Vector3 inputs = Vector3.zero;
		private bool isGrounded = true;
		private bool isWall = false;
	
		private Vector3 moveDirection = Vector3.zero;
		private Transform myTransform;

		public float speed;

		public bool throneCollision;

	private bool jumpCooldown;

	public float bounceHeight;

	public float maxHeight;

	private bool isBouncePad = false;

	private bool ascender = false;
	private Vector3 normal;
	public float wallBounceHeight;
	public float bouncePadHeight;

	void Start()
		{
		body = GetComponent<Rigidbody>();
		myTransform = transform;
		throneCollision = false;
	}

	void Update()
	{

		//Movement Code
		float inputsX = Input.GetAxis("Horizontal");
		float inputsY = Input.GetAxis("Vertical");

		moveDirection.x = inputsX * speed;
		moveDirection.z = inputsY * speed;
		moveDirection = myTransform.TransformDirection(moveDirection);

		if (inputs != Vector3.zero)	
		transform.forward = inputs;

		//CHECK WHAT UR TOUCHING
		//Wall Bounce
		if (isWall)
		{
			Debug.Log("Wall");
			body.AddForce(normal * wallBounceHeight, ForceMode.Impulse);
		}

		//Bounce Pad
		if (isBouncePad)
		{
			Debug.Log("Bounce Pad");
			body.AddForce(normal * bouncePadHeight, ForceMode.Impulse);
		}

		//Ground Bounce
		if (isGrounded)
		{
			//Add force to jump normally
			body.AddForce(normal * bounceHeight, ForceMode.Impulse);

			/*
			 //OLD CODE LEAVE IN CASE OF FUCK UP
			if (Input.GetButtonDown("Jump")){
				body.AddForce(Vector3.up * Mathf.Sqrt(JumpHeight * -2f * Physics.gravity.y), ForceMode.VelocityChange);
				//jumpCooldown = true;
			}
			*/
		}

		//Check for jump
		if (Input.GetButton("Jump") && jumpCooldown == true)
		{
			jumpCooldown = false;
		}

		//Apply jump force
		if (jumpCooldown == false)
		{
			if (isGrounded)
			{
				body.AddForce(normal*JumpHeight, ForceMode.Impulse);
				jumpCooldown = true;
			}
			if (isBouncePad)
			{
				Debug.Log("Bounce Jump");
				body.AddForce(normal*4, ForceMode.Impulse);
				jumpCooldown = true;
			}
		}


		//Height limit
		if (body.position.y > maxHeight && ascender == false)
		{
			body.AddForce(Vector3.down, ForceMode.VelocityChange);
		}

		//Throne Super Float
		if (throneCollision)
		{
			print("Throne Hit");
			if (Input.GetButtonDown("Jump"))
			{
				ascender = true;
				body.AddForce(Vector3.up * (JumpHeight * 10f), ForceMode.VelocityChange);
				body.useGravity = false;
			}
		}
	}


		void FixedUpdate()
		{	
		body.MovePosition(body.position + moveDirection * Time.fixedDeltaTime);
	}



	private void OnCollisionEnter(Collision collision)

	{
		if (collision.gameObject.tag == "Ground")
		{
			isGrounded = true;
		}

		if(collision.gameObject.tag == "Throne")
		{
			throneCollision = true;
		}

		if (collision.gameObject.tag == "Wall")
		{
			isWall = true;
		}

		if (collision.gameObject.tag == "Bounce")
		{
			isBouncePad = true;
		}

		normal = collision.GetContact(0).normal;
		print("Collision normal " + normal);
	}

	private void OnCollisionExit(Collision collision)
	{
		if (collision.gameObject.tag == "Ground")
		{
			isGrounded = false;
		}

		if (collision.gameObject.tag == "Throne")
		{
			throneCollision = false;
		}

		if (collision.gameObject.tag == "Wall")
		{
			isWall = false;
		}

		if (collision.gameObject.tag == "Bounce")
		{
			isBouncePad = false;
		}
	}
}