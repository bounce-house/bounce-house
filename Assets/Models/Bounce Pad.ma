//Maya ASCII 2020 scene
//Name: Bounce Pad.ma
//Last modified: Fri, Sep 11, 2020 03:42:30 PM
//Codeset: 1252
requires maya "2020";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "201911140446-42a737a01c";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18363)\n";
fileInfo "UUID" "87B89925-4AE0-2127-684E-C28F2BFC81F4";
fileInfo "license" "student";
createNode transform -n "pCylinder14";
	rename -uid "7F2CF808-4A29-7E28-29E5-169AA578872B";
	setAttr ".t" -type "double3" -9.1511790107048618 1.1906445707847491 -6.9520781219621162 ;
	setAttr ".s" -type "double3" 0.74837088043558997 0.27855566433147377 0.74837088043558997 ;
createNode mesh -n "pCylinderShape14" -p "pCylinder14";
	rename -uid "21B399DF-4DAB-3387-3047-37B0F66BFA9C";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.84375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".pt";
	setAttr ".pt[80]" -type "float3" -0.028077696 0 -0.01163016 ;
	setAttr ".pt[81]" -type "float3" -0.060548518 1.110223e-16 -0.025080008 ;
	setAttr ".pt[82]" -type "float3" -0.028077696 0 0.011630163 ;
	setAttr ".pt[83]" -type "float3" -0.060548518 1.110223e-16 0.02508002 ;
	setAttr ".pt[84]" -type "float3" -0.011630161 0 0.028077696 ;
	setAttr ".pt[85]" -type "float3" -0.025080008 1.110223e-16 0.060548518 ;
	setAttr ".pt[86]" -type "float3" 0.011630163 0 0.028077696 ;
	setAttr ".pt[87]" -type "float3" 0.02508002 1.110223e-16 0.060548518 ;
	setAttr ".pt[88]" -type "float3" 0.028077696 0 0.011630163 ;
	setAttr ".pt[89]" -type "float3" 0.060548518 1.110223e-16 0.02508002 ;
	setAttr ".pt[90]" -type "float3" 0.028077696 0 -0.01163016 ;
	setAttr ".pt[91]" -type "float3" 0.060548518 1.110223e-16 -0.025080008 ;
	setAttr ".pt[92]" -type "float3" 0.011630163 0 -0.028077696 ;
	setAttr ".pt[93]" -type "float3" 0.02508002 1.110223e-16 -0.060548518 ;
	setAttr ".pt[94]" -type "float3" -0.011630161 0 -0.028077696 ;
	setAttr ".pt[95]" -type "float3" -0.025080008 1.110223e-16 -0.060548518 ;
	setAttr ".pt[104]" -type "float3" 0.037473004 0 0.024236085 ;
	setAttr ".pt[105]" -type "float3" 0.043634947 0 0.0093599157 ;
	setAttr ".pt[106]" -type "float3" 0.043634947 0 -0.0093599083 ;
	setAttr ".pt[107]" -type "float3" 0.037473004 0 -0.024236083 ;
	setAttr ".pt[108]" -type "float3" 0.024236085 0 -0.037472997 ;
	setAttr ".pt[109]" -type "float3" 0.0093599157 0 -0.043634947 ;
	setAttr ".pt[110]" -type "float3" -0.0093599092 0 -0.043634947 ;
	setAttr ".pt[111]" -type "float3" -0.024236085 0 -0.037472997 ;
	setAttr ".pt[112]" -type "float3" -0.037473004 0 -0.024236083 ;
	setAttr ".pt[113]" -type "float3" -0.043634947 0 -0.0093599083 ;
	setAttr ".pt[114]" -type "float3" -0.043634947 0 0.0093599157 ;
	setAttr ".pt[115]" -type "float3" -0.037473004 0 0.024236085 ;
	setAttr ".pt[116]" -type "float3" -0.024236085 0 0.037473004 ;
	setAttr ".pt[117]" -type "float3" -0.0093599092 0 0.043634947 ;
	setAttr ".pt[118]" -type "float3" 0.0093599157 0 0.043634947 ;
	setAttr ".pt[119]" -type "float3" 0.024236085 0 0.037473004 ;
createNode mesh -n "polySurfaceShape2" -p "pCylinder14";
	rename -uid "086846D2-4EA7-DD0D-F715-0A8D355E3D34";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.84375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 97 ".uvst[0].uvsp[0:96]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.625 0.38768798 0.375 0.38768798 0.59375
		 0.38768798 0.5625 0.38768798 0.53125 0.38768798 0.5 0.38768798 0.46875 0.38768798
		 0.4375 0.38768798 0.40625003 0.38768798 0.625 0.47791356 0.375 0.47791356 0.59375
		 0.47791356 0.5625 0.47791356 0.53125 0.47791356 0.5 0.47791356 0.46875 0.47791356
		 0.4375 0.47791356 0.40625 0.47791356 0.625 0.56212407 0.375 0.56212407 0.59375 0.56212407
		 0.5625 0.56212407 0.53125 0.56212407 0.5 0.56212407 0.46875 0.56212407 0.4375 0.56212407
		 0.40625 0.56212407 0.625 0.63791353 0.375 0.63791353 0.59375 0.63791353 0.5625 0.63791353
		 0.53125 0.63791353 0.5 0.63791353 0.46875 0.63791353 0.4375 0.63791353 0.40625 0.63791353
		 0.625 0.6000188 0.375 0.6000188 0.40625 0.6000188 0.4375 0.6000188 0.46875 0.6000188
		 0.5 0.6000188 0.53125 0.6000188 0.5625 0.6000188 0.59375 0.6000188 0.625 0.51159775
		 0.375 0.51159775 0.40625 0.51159775 0.4375 0.51159775 0.46875 0.51159775 0.5 0.51159775
		 0.53125 0.51159775 0.5625 0.51159775 0.59375 0.51159775 0.625 0.42377824 0.375 0.42377824
		 0.40625003 0.42377824 0.4375 0.42377824 0.46875 0.42377824 0.5 0.42377824 0.53125
		 0.42377824 0.5625 0.42377824 0.59375 0.42377824;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 64 ".pt[8:71]" -type "float3"  0 -0.36456969 0 0 -0.36456969 
		0 0 -0.36456969 0 0 -0.36456969 0 0 -0.36456969 0 0 -0.36456969 0 0 -0.36456969 0 
		1.4432899e-15 -0.36456969 0 0.045145411 -0.065561339 -0.045145411 0.063845254 -0.065561339 
		-3.5713572e-09 0.045145411 -0.065561339 0.045145411 -3.8054737e-09 -0.065561339 0.063845247 
		-0.045145411 -0.065561339 0.045145411 -0.063845254 -0.065561339 -3.5713572e-09 -0.045145411 
		-0.065561339 -0.045145411 -3.8054737e-09 -0.065561339 -0.063845247 0.045145411 -0.034915622 
		-0.045145411 0.063845254 -0.034915622 -3.5713572e-09 0.045145411 -0.034915622 0.045145411 
		-3.8054737e-09 -0.034915622 0.06384524 -0.045145411 -0.034915622 0.045145411 -0.063845254 
		-0.034915622 -3.5713572e-09 -0.045145411 -0.034915622 -0.045145411 -3.8054737e-09 
		-0.034915622 -0.063845247 0.045145411 -0.0063129519 -0.045145411 0.063845254 -0.0063129519 
		-3.5713572e-09 0.045145411 -0.0063129519 0.045145411 -3.8054737e-09 -0.0063129519 
		0.063845247 -0.045145411 -0.0063129519 0.045145411 -0.063845254 -0.0063129519 -3.5713572e-09 
		-0.045145411 -0.0063129519 -0.045145411 -3.8054737e-09 -0.0063129519 -0.063845247 
		0.045145411 0.019429462 -0.045145411 0.063845254 0.019429462 -3.5713572e-09 0.045145411 
		0.019429462 0.045145411 -3.8054737e-09 0.019429462 0.06384524 -0.045145411 0.019429462 
		0.045145411 -0.063845254 0.019429462 -3.5713572e-09 -0.045145411 0.019429462 -0.045145411 
		-3.8054737e-09 0.019429462 -0.063845247 -0.042353563 -0.028079715 0.042353563 3.5701393e-09 
		-0.028079715 0.059896979 0.042353563 -0.028079715 0.042353563 0.059896998 -0.028079715 
		2.1963802e-10 0.042353563 -0.028079715 -0.04235357 3.5701393e-09 -0.028079715 -0.059896994 
		-0.042353563 -0.028079715 -0.04235357 -0.059896998 -0.028079715 2.1963802e-10 -0.042353563 
		9.5834606e-05 0.042353563 3.5701393e-09 9.5834606e-05 0.059896994 0.042353563 9.5834606e-05 
		0.042353563 0.059896998 9.5834606e-05 2.1963802e-10 0.042353563 9.5834606e-05 -0.04235357 
		3.5701393e-09 9.5834606e-05 -0.059896994 -0.042353563 9.5834606e-05 -0.04235357 -0.059896998 
		9.5834606e-05 2.1963802e-10 -0.042353563 0.028079715 0.042353563 3.5701393e-09 0.028079715 
		0.059896994 0.042353563 0.028079715 0.042353563 0.059896998 0.028079715 2.1963802e-10 
		0.042353563 0.028079715 -0.04235357 3.5701393e-09 0.028079715 -0.059896994 -0.042353563 
		0.028079715 -0.04235357 -0.059896998 0.028079715 2.1963802e-10;
	setAttr -s 72 ".vt[0:71]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0.80631745 -0.69338709 -0.80631733 1.14030516 -0.69338709 -4.1814174e-09 0.80631745 -0.69338709 0.80631745
		 -8.3628349e-09 -0.69338709 1.14030504 -0.80631733 -0.69338709 0.80631745 -1.14030504 -0.69338709 -4.1814174e-09
		 -0.80631733 -0.69338709 -0.80631733 -8.3628349e-09 -0.69338709 -1.14030492 0.80631745 -0.1460406 -0.80631733
		 1.14030516 -0.1460406 -4.1814174e-09 0.80631745 -0.1460406 0.80631745 -8.3628349e-09 -0.1460406 1.14030492
		 -0.80631733 -0.1460406 0.80631745 -1.14030504 -0.1460406 -4.1814174e-09 -0.80631733 -0.1460406 -0.80631733
		 -8.3628349e-09 -0.1460406 -1.14030492 0.80631745 0.36481604 -0.80631733 1.14030516 0.36481604 -4.1814174e-09
		 0.80631745 0.36481604 0.80631745 -8.3628349e-09 0.36481604 1.14030504 -0.80631733 0.36481604 0.80631745
		 -1.14030504 0.36481604 -4.1814174e-09 -0.80631733 0.36481604 -0.80631733 -8.3628349e-09 0.36481604 -1.14030492
		 0.80631745 0.82458711 -0.80631733 1.14030516 0.82458711 -4.1814174e-09 0.80631745 0.82458711 0.80631745
		 -8.3628349e-09 0.82458711 1.14030492 -0.80631733 0.82458711 0.80631745 -1.14030504 0.82458711 -4.1814174e-09
		 -0.80631733 0.82458711 -0.80631733 -8.3628349e-09 0.82458711 -1.14030492 0.80631745 0.59470159 -0.80631733
		 -8.3628349e-09 0.59470159 -1.14030492 -0.80631733 0.59470159 -0.80631733 -1.14030504 0.59470159 -4.1814174e-09
		 -0.80631733 0.59470159 0.80631745 -8.3628349e-09 0.59470159 1.14030504 0.80631745 0.59470159 0.80631745
		 1.14030516 0.59470159 -4.1814174e-09 0.80631745 0.058302052 -0.80631733 -8.3628349e-09 0.058302052 -1.14030504
		 -0.80631733 0.058302052 -0.80631733 -1.14030504 0.058302052 -4.1814174e-09 -0.80631733 0.058302052 0.80631745
		 -8.3628349e-09 0.058302052 1.14030504 0.80631745 0.058302052 0.80631745 1.14030516 0.058302052 -4.1814174e-09
		 0.80631745 -0.47444853 -0.80631733 -8.3628349e-09 -0.47444853 -1.14030504 -0.80631733 -0.47444853 -0.80631733
		 -1.14030504 -0.47444853 -4.1814174e-09 -0.80631733 -0.47444853 0.80631745 -8.3628349e-09 -0.47444853 1.14030504
		 0.80631745 -0.47444853 0.80631745 1.14030516 -0.47444853 -4.1814174e-09;
	setAttr -s 136 ".ed[0:135]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 16 0 1 23 0 2 22 0
		 3 21 0 4 20 0 5 19 0 6 18 0 7 17 0 16 64 0 17 71 0 18 70 0 19 69 0 20 68 0 21 67 0
		 22 66 0 23 65 0 16 17 1 17 18 1 18 19 1 19 20 1 20 21 1 21 22 1 22 23 1 23 16 1 24 56 0
		 25 63 0 26 62 0 27 61 0 28 60 0 29 59 0 30 58 0 31 57 0 24 25 1 25 26 1 26 27 1 27 28 1
		 28 29 1 29 30 1 30 31 1 31 24 1 32 48 0 33 55 0 34 54 0 35 53 0 36 52 0 37 51 0 38 50 0
		 39 49 0 32 33 1 33 34 1 34 35 1 35 36 1 36 37 1 37 38 1 38 39 1 39 32 1 40 8 0 41 15 0
		 42 14 0 43 13 0 44 12 0 45 11 0 46 10 0 47 9 0 40 41 1 41 42 1 42 43 1 43 44 1 44 45 1
		 45 46 1 46 47 1 47 40 1 48 40 0 49 47 0 50 46 0 51 45 0 52 44 0 53 43 0 54 42 0 55 41 0
		 48 49 1 49 50 1 50 51 1 51 52 1 52 53 1 53 54 1 54 55 1 55 48 1 56 32 0 57 39 0 58 38 0
		 59 37 0 60 36 0 61 35 0 62 34 0 63 33 0 56 57 1 57 58 1 58 59 1 59 60 1 60 61 1 61 62 1
		 62 63 1 63 56 1 64 24 0 65 31 0 66 30 0 67 29 0 68 28 0 69 27 0 70 26 0 71 25 0 64 65 1
		 65 66 1 66 67 1 67 68 1 68 69 1 69 70 1 70 71 1 71 64 1;
	setAttr -s 66 -ch 272 ".fc[0:65]" -type "polyFaces" 
		f 4 0 17 39 -17
		mu 0 4 8 9 42 35
		f 4 1 18 38 -18
		mu 0 4 9 10 41 42
		f 4 2 19 37 -19
		mu 0 4 10 11 40 41
		f 4 3 20 36 -20
		mu 0 4 11 12 39 40
		f 4 4 21 35 -21
		mu 0 4 12 13 38 39
		f 4 5 22 34 -22
		mu 0 4 13 14 37 38
		f 4 6 23 33 -23
		mu 0 4 14 15 36 37
		f 4 7 16 32 -24
		mu 0 4 15 16 34 36
		f 8 -8 -7 -6 -5 -4 -3 -2 -1
		mu 0 8 0 7 6 5 4 3 2 1
		f 8 8 9 10 11 12 13 14 15
		mu 0 8 32 31 30 29 28 27 26 33
		f 4 135 120 48 -128
		mu 0 4 96 88 43 45
		f 4 134 127 49 -127
		mu 0 4 95 96 45 46
		f 4 133 126 50 -126
		mu 0 4 94 95 46 47
		f 4 132 125 51 -125
		mu 0 4 93 94 47 48
		f 4 131 124 52 -124
		mu 0 4 92 93 48 49
		f 4 130 123 53 -123
		mu 0 4 91 92 49 50
		f 4 129 122 54 -122
		mu 0 4 90 91 50 51
		f 4 128 121 55 -121
		mu 0 4 89 90 51 44
		f 4 119 104 64 -112
		mu 0 4 87 79 52 54
		f 4 118 111 65 -111
		mu 0 4 86 87 54 55
		f 4 117 110 66 -110
		mu 0 4 85 86 55 56
		f 4 116 109 67 -109
		mu 0 4 84 85 56 57
		f 4 115 108 68 -108
		mu 0 4 83 84 57 58
		f 4 114 107 69 -107
		mu 0 4 82 83 58 59
		f 4 113 106 70 -106
		mu 0 4 81 82 59 60
		f 4 112 105 71 -105
		mu 0 4 80 81 60 53
		f 4 103 88 80 -96
		mu 0 4 78 70 61 63
		f 4 102 95 81 -95
		mu 0 4 77 78 63 64
		f 4 101 94 82 -94
		mu 0 4 76 77 64 65
		f 4 100 93 83 -93
		mu 0 4 75 76 65 66
		f 4 99 92 84 -92
		mu 0 4 74 75 66 67
		f 4 98 91 85 -91
		mu 0 4 73 74 67 68
		f 4 97 90 86 -90
		mu 0 4 72 73 68 69
		f 4 96 89 87 -89
		mu 0 4 71 72 69 62
		f 4 -81 72 -16 -74
		mu 0 4 63 61 25 24
		f 4 -82 73 -15 -75
		mu 0 4 64 63 24 23
		f 4 -83 74 -14 -76
		mu 0 4 65 64 23 22
		f 4 -84 75 -13 -77
		mu 0 4 66 65 22 21
		f 4 -85 76 -12 -78
		mu 0 4 67 66 21 20
		f 4 -86 77 -11 -79
		mu 0 4 68 67 20 19
		f 4 -87 78 -10 -80
		mu 0 4 69 68 19 18
		f 4 -88 79 -9 -73
		mu 0 4 62 69 18 17
		f 4 -72 63 -97 -57
		mu 0 4 53 60 72 71
		f 4 -71 62 -98 -64
		mu 0 4 60 59 73 72
		f 4 -70 61 -99 -63
		mu 0 4 59 58 74 73
		f 4 -69 60 -100 -62
		mu 0 4 58 57 75 74
		f 4 -68 59 -101 -61
		mu 0 4 57 56 76 75
		f 4 -67 58 -102 -60
		mu 0 4 56 55 77 76
		f 4 -66 57 -103 -59
		mu 0 4 55 54 78 77
		f 4 -65 56 -104 -58
		mu 0 4 54 52 70 78
		f 4 -56 47 -113 -41
		mu 0 4 44 51 81 80
		f 4 -55 46 -114 -48
		mu 0 4 51 50 82 81
		f 4 -54 45 -115 -47
		mu 0 4 50 49 83 82
		f 4 -53 44 -116 -46
		mu 0 4 49 48 84 83
		f 4 -52 43 -117 -45
		mu 0 4 48 47 85 84
		f 4 -51 42 -118 -44
		mu 0 4 47 46 86 85
		f 4 -50 41 -119 -43
		mu 0 4 46 45 87 86
		f 4 -49 40 -120 -42
		mu 0 4 45 43 79 87
		f 4 -40 31 -129 -25
		mu 0 4 35 42 90 89
		f 4 -39 30 -130 -32
		mu 0 4 42 41 91 90
		f 4 -38 29 -131 -31
		mu 0 4 41 40 92 91
		f 4 -37 28 -132 -30
		mu 0 4 40 39 93 92
		f 4 -36 27 -133 -29
		mu 0 4 39 38 94 93
		f 4 -35 26 -134 -28
		mu 0 4 38 37 95 94
		f 4 -34 25 -135 -27
		mu 0 4 37 36 96 95
		f 4 -33 24 -136 -26
		mu 0 4 36 34 88 96;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode groupId -n "groupId17";
	rename -uid "865F4C53-4BD3-4AA5-7D55-A0A8C97CD871";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "39C47F24-4B2D-79C6-6D04-15996CC3EB2D";
	setAttr ".ihi" 0;
createNode shadingEngine -n "blinn1SG";
	rename -uid "F4B3E5CB-46D8-BD9E-B28F-5FAB8F50667B";
	setAttr ".ihi" 0;
	setAttr -s 15 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 15 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "7BC090EB-45D8-BEA7-392A-45A9041AFF69";
createNode blinn -n "Shiny";
	rename -uid "CC1DF5C6-4F57-3163-7C80-BCA9EA459D18";
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 0.52597404 0.16804831 0 ;
	setAttr ".ambc" -type "float3" 0.87970001 1 0 ;
	setAttr ".ic" -type "float3" 0.012987013 0.012987013 0.012987013 ;
	setAttr ".rfl" 0.45454546809196472;
	setAttr ".rc" -type "float3" 0.1934 0.0113 0.1725 ;
createNode groupParts -n "groupParts4";
	rename -uid "945E94F0-4C12-CCAD-F04E-B0852C0BB3A2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[9]";
createNode groupParts -n "groupParts3";
	rename -uid "62084A63-404C-C210-8E14-4FB952B32F8F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0:8]" "f[10:97]";
	setAttr ".irc" -type "componentList" 1 "f[9]";
createNode polySplit -n "polySplit49";
	rename -uid "207512E1-4629-6BBA-CAF5-AAAF1191AF38";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483487;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit48";
	rename -uid "7FD97FED-4A45-07D1-6B87-3EA60EAC6F7F";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483500;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit47";
	rename -uid "6F0BD701-4AEF-2514-1E23-D2B1080BE5C5";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483490;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit46";
	rename -uid "6DA133E8-4FCE-271E-736C-79B881C09633";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483502;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit45";
	rename -uid "D696E187-4B36-9ECA-8C73-DA86F63B9CD3";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483493;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit44";
	rename -uid "DAF803CC-4805-BA79-8B4E-109B01DC5F50";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483504;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit43";
	rename -uid "98359918-4457-3001-ECF3-61A2DC8B7341";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483496;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit42";
	rename -uid "FD497632-4415-49E9-6D79-D99FF4D31BD1";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483506;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit41";
	rename -uid "2A69613A-4AEE-D137-C82B-C4AA885A4409";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483475;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit40";
	rename -uid "EFCE85C9-4CB1-8246-FA4C-5193F4B1E24F";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483508;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit39";
	rename -uid "60F747A3-44A0-5126-6157-C1A0CF7DB277";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483478;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit38";
	rename -uid "4619B920-4693-E903-ABD0-0693189632A6";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483510;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit37";
	rename -uid "2B901771-4BE1-C029-E424-469D3233E2AF";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483481;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit36";
	rename -uid "412C7F16-4EB3-23F7-399F-1B8CC5641C64";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483497;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit35";
	rename -uid "CA6566A7-461A-47AB-7C22-8B9834F7D570";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483484;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit34";
	rename -uid "3CD37731-4CBE-AFDF-0851-E5BC9257BEDD";
	setAttr ".e[0]"  0.5;
	setAttr ".d[0]"  -2147483498;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "EE3FF066-4D89-FF99-D3A6-46AF4A414D19";
	setAttr ".uopa" yes;
	setAttr -s 32 ".tk";
	setAttr ".tk[8]" -type "float3" -0.049533561 0 0.016978705 ;
	setAttr ".tk[9]" -type "float3" -4.1753672e-09 0 0.024011429 ;
	setAttr ".tk[10]" -type "float3" 0.049533561 0 0.016978705 ;
	setAttr ".tk[11]" -type "float3" 0.070051029 0 -7.1559808e-10 ;
	setAttr ".tk[12]" -type "float3" 0.049533561 0 -0.016978705 ;
	setAttr ".tk[13]" -type "float3" -4.1753672e-09 0 -0.024011446 ;
	setAttr ".tk[14]" -type "float3" -0.049533561 0 -0.016978705 ;
	setAttr ".tk[15]" -type "float3" -0.070051029 0 -7.1559808e-10 ;
	setAttr ".tk[80]" -type "float3" 0.089249454 0 0.036968324 ;
	setAttr ".tk[81]" -type "float3" -0.07032381 -0.018550185 -0.029129056 ;
	setAttr ".tk[82]" -type "float3" 0.089249454 0 -0.036968336 ;
	setAttr ".tk[83]" -type "float3" -0.07032381 -0.018550185 0.029129058 ;
	setAttr ".tk[84]" -type "float3" 0.036968328 0 -0.089249454 ;
	setAttr ".tk[85]" -type "float3" -0.029129058 -0.018550185 0.070323803 ;
	setAttr ".tk[86]" -type "float3" -0.036968336 0 -0.089249454 ;
	setAttr ".tk[87]" -type "float3" 0.029129058 -0.018550185 0.070323803 ;
	setAttr ".tk[88]" -type "float3" -0.089249454 0 -0.036968336 ;
	setAttr ".tk[89]" -type "float3" 0.070323803 -0.018550185 0.029129058 ;
	setAttr ".tk[90]" -type "float3" -0.089249454 0 0.036968324 ;
	setAttr ".tk[91]" -type "float3" 0.070323803 -0.018550185 -0.029129056 ;
	setAttr ".tk[92]" -type "float3" -0.036968336 0 0.089249454 ;
	setAttr ".tk[93]" -type "float3" 0.029129058 -0.018550185 -0.070323803 ;
	setAttr ".tk[94]" -type "float3" 0.036968328 0 0.089249454 ;
	setAttr ".tk[95]" -type "float3" -0.029129058 -0.018550185 -0.070323803 ;
	setAttr ".tk[96]" -type "float3" -0.057058021 0.012643066 0.057058014 ;
	setAttr ".tk[97]" -type "float3" 4.8096331e-09 0.012643066 0.080692254 ;
	setAttr ".tk[98]" -type "float3" 0.057058014 0.012643066 0.057058014 ;
	setAttr ".tk[99]" -type "float3" 0.080692261 0.012643066 2.4048168e-09 ;
	setAttr ".tk[100]" -type "float3" 0.057058014 0.012643066 -0.057058014 ;
	setAttr ".tk[101]" -type "float3" 4.8096331e-09 0.012643066 -0.080692254 ;
	setAttr ".tk[102]" -type "float3" -0.054536141 0.018550185 -0.054536134 ;
	setAttr ".tk[103]" -type "float3" -0.080692261 0.012643066 2.4048168e-09 ;
createNode polySplit -n "polySplit33";
	rename -uid "395014E9-4EA8-B807-1041-69A5EE0029D0";
	setAttr -s 17 ".e[0:16]"  0 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.5 1 0.57457799
		 1 0.5 1;
	setAttr -s 17 ".d[0:16]"  -2147483492 -2147483505 -2147483636 -2147483503 -2147483635 -2147483501 
		-2147483634 -2147483499 -2147483633 -2147483512 -2147483640 -2147483511 -2147483639 -2147483509 -2147483638 -2147483507 -2147483637;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "22FBBB45-454E-2172-D359-5680B058891A";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[72:95]" -type "float3"  -0.32327399 2.220446e-16 0.32327402
		 -2.7249934e-08 2.220446e-16 0.45717797 0.32327402 2.220446e-16 0.32327402 0.45717794
		 2.220446e-16 -1.3624967e-08 0.32327402 2.220446e-16 -0.32327402 -2.7249934e-08 2.220446e-16
		 -0.45717791 -0.32327399 2.220446e-16 -0.32327402 -0.45717794 2.220446e-16 -1.3624967e-08
		 0.18126397 -0.080402598 0.075082012 0.18126397 0.080402598 0.075082012 0.18126397
		 -0.080402598 -0.075082034 0.18126397 0.080402598 -0.075082034 0.075082034 -0.080402598
		 -0.18126395 0.075082034 0.080402598 -0.18126395 -0.075082041 -0.080402598 -0.18126395
		 -0.075082041 0.080402598 -0.18126395 -0.18126397 -0.080402598 -0.075082034 -0.18126397
		 0.080402598 -0.075082034 -0.18126397 -0.080402598 0.075082012 -0.18126397 0.080402598
		 0.075082012 -0.075082041 -0.080402598 0.18126395 -0.075082041 0.080402598 0.18126395
		 0.075082034 -0.080402598 0.18126395 0.075082034 0.080402598 0.18126395;
createNode polySplit -n "polySplit32";
	rename -uid "2224BDED-4DCC-DCA2-D8D9-F8827D7126C4";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483508 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit31";
	rename -uid "501DD021-436B-D466-42D3-9EB5DBD66AC4";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483510 -2147483640;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "A3DDA5C0-4441-BDBF-7F41-B1947BC7E132";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483497 -2147483633;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit29";
	rename -uid "E44A8A27-4684-EFF3-F956-009A961C7283";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483498 -2147483634;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit28";
	rename -uid "172AA73A-4473-8732-0C99-FEB0516B017B";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483500 -2147483635;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit27";
	rename -uid "8E27ABDB-4022-D122-367E-74926CF84E05";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483502 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit26";
	rename -uid "94A09CDA-48B9-93E5-C047-BD881A0CED37";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483504 -2147483637;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit25";
	rename -uid "8E5B823E-43AB-99D2-5AA8-E0981D2E1A23";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483506 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "B3E9D05D-494D-BA2B-2082-1593C7E46316";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[72:79]" -type "float3"  0 0.75721473 0 0 0.75721473
		 0 0 0.75721473 0 0 0.75721473 0 0 0.75721473 0 0 0.75721473 0 0 0.75721473 0 0 0.75721473
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "348F7E9E-4E20-D7E9-EE85-22989452679F";
	setAttr ".ics" -type "componentList" 1 "f[9]";
	setAttr ".ix" -type "matrix" 2.428618273560355 0 0 0 0 0.90397073735071165 0 0 0 0 2.428618273560355 0
		 -5.9377164918490042 1.1906445707847491 1.6483693495483251 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.9377165 1.7650549 1.6483694 ;
	setAttr ".rs" 53925;
	setAttr ".ls" -type "double3" 1 1 1.136073204013569 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.3663344758954992 1.7650550001523553 -0.78024863449817072 ;
	setAttr ".cbx" -type "double3" -3.5090982182886492 1.7650550001523553 4.0769874783517501 ;
createNode groupId -n "groupId18";
	rename -uid "9599C55D-4D4E-693C-CC96-EFACD5896083";
	setAttr ".ihi" 0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "4A271797-494B-51C3-1B8D-DCBD649E195E";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 54 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 50 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId17.id" "pCylinderShape14.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape14.iog.og[0].gco";
connectAttr "groupId19.id" "pCylinderShape14.iog.og[1].gid";
connectAttr "blinn1SG.mwc" "pCylinderShape14.iog.og[1].gco";
connectAttr "groupParts4.og" "pCylinderShape14.i";
connectAttr "groupId18.id" "pCylinderShape14.ciog.cog[0].cgid";
connectAttr "Shiny.oc" "blinn1SG.ss";
connectAttr "pCylinderShape8.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape8.ciog.cog[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape7.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape7.ciog.cog[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape6.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape6.ciog.cog[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape5.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape5.ciog.cog[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape4.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape4.ciog.cog[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinder13Shape.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinderShape14.iog.og[1]" "blinn1SG.dsm" -na;
connectAttr "pCylinder16Shape.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinder17Shape.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "pCylinder15Shape.iog.og[0]" "blinn1SG.dsm" -na;
connectAttr "groupId6.msg" "blinn1SG.gn" -na;
connectAttr "groupId7.msg" "blinn1SG.gn" -na;
connectAttr "groupId8.msg" "blinn1SG.gn" -na;
connectAttr "groupId9.msg" "blinn1SG.gn" -na;
connectAttr "groupId10.msg" "blinn1SG.gn" -na;
connectAttr "groupId11.msg" "blinn1SG.gn" -na;
connectAttr "groupId12.msg" "blinn1SG.gn" -na;
connectAttr "groupId13.msg" "blinn1SG.gn" -na;
connectAttr "groupId14.msg" "blinn1SG.gn" -na;
connectAttr "groupId15.msg" "blinn1SG.gn" -na;
connectAttr "groupId16.msg" "blinn1SG.gn" -na;
connectAttr "groupId19.msg" "blinn1SG.gn" -na;
connectAttr "groupId51.msg" "blinn1SG.gn" -na;
connectAttr "groupId52.msg" "blinn1SG.gn" -na;
connectAttr "groupId53.msg" "blinn1SG.gn" -na;
connectAttr "blinn1SG.msg" "materialInfo2.sg";
connectAttr "Shiny.msg" "materialInfo2.m";
connectAttr "groupParts3.og" "groupParts4.ig";
connectAttr "groupId19.id" "groupParts4.gi";
connectAttr "polySplit49.out" "groupParts3.ig";
connectAttr "groupId17.id" "groupParts3.gi";
connectAttr "polySplit48.out" "polySplit49.ip";
connectAttr "polySplit47.out" "polySplit48.ip";
connectAttr "polySplit46.out" "polySplit47.ip";
connectAttr "polySplit45.out" "polySplit46.ip";
connectAttr "polySplit44.out" "polySplit45.ip";
connectAttr "polySplit43.out" "polySplit44.ip";
connectAttr "polySplit42.out" "polySplit43.ip";
connectAttr "polySplit41.out" "polySplit42.ip";
connectAttr "polySplit40.out" "polySplit41.ip";
connectAttr "polySplit39.out" "polySplit40.ip";
connectAttr "polySplit38.out" "polySplit39.ip";
connectAttr "polySplit37.out" "polySplit38.ip";
connectAttr "polySplit36.out" "polySplit37.ip";
connectAttr "polySplit35.out" "polySplit36.ip";
connectAttr "polySplit34.out" "polySplit35.ip";
connectAttr "polyTweak7.out" "polySplit34.ip";
connectAttr "polySplit33.out" "polyTweak7.ip";
connectAttr "polyTweak6.out" "polySplit33.ip";
connectAttr "polySplit32.out" "polyTweak6.ip";
connectAttr "polySplit31.out" "polySplit32.ip";
connectAttr "polySplit30.out" "polySplit31.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit28.out" "polySplit29.ip";
connectAttr "polySplit27.out" "polySplit28.ip";
connectAttr "polySplit26.out" "polySplit27.ip";
connectAttr "polySplit25.out" "polySplit26.ip";
connectAttr "polyTweak5.out" "polySplit25.ip";
connectAttr "polyExtrudeFace3.out" "polyTweak5.ip";
connectAttr "polySurfaceShape2.o" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape14.wm" "polyExtrudeFace3.mp";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "Shiny.msg" ":defaultShaderList1.s" -na;
connectAttr "pCylinderShape14.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape14.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId17.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId18.msg" ":initialShadingGroup.gn" -na;
// End of Bounce Pad.ma
