//Maya ASCII 2020 scene
//Name: CornerPillar1.ma
//Last modified: Tue, Sep 08, 2020 05:51:37 PM
//Codeset: 1252
requires maya "2020";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "201911140446-42a737a01c";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18363)\n";
fileInfo "UUID" "6DDAB82B-44CE-D593-AB88-45B444E52519";
fileInfo "license" "student";
createNode transform -n "pCylinder9";
	rename -uid "F5F77092-452E-A02E-C1BE-1BB23F190DBE";
	setAttr ".t" -type "double3" 8.695006715118911 6.4439221116556098 -1.3482956707571816 ;
	setAttr ".s" -type "double3" 2.428618273560355 6.2156743649956505 2.428618273560355 ;
createNode transform -n "transform1" -p "pCylinder9";
	rename -uid "83F187C8-4D9C-D33F-E303-1DB6092BB808";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape9" -p "transform1";
	rename -uid "55023771-4D0D-41C6-C1C9-9B9DE5497E52";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:31]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5625 0.53686091303825378 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 45 ".uvst[0].uvsp[0:44]" -type "float2" 0.46875 0.3125 0.5
		 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.59375 0.38768798 0.5625
		 0.38768798 0.53125 0.38768798 0.5 0.38768798 0.46875 0.38768798 0.59375 0.47791356
		 0.5625 0.47791356 0.53125 0.47791356 0.5 0.47791356 0.46875 0.47791356 0.59375 0.56212407
		 0.5625 0.56212407 0.53125 0.56212407 0.5 0.56212407 0.46875 0.56212407 0.59375 0.63791353
		 0.5625 0.63791353 0.53125 0.63791353 0.5 0.63791353 0.46875 0.63791353 0.46875 0.6000188
		 0.5 0.6000188 0.53125 0.6000188 0.5625 0.6000188 0.59375 0.6000188 0.46875 0.51159775
		 0.5 0.51159775 0.53125 0.51159775 0.5625 0.51159775 0.59375 0.51159775 0.46875 0.42377824
		 0.5 0.42377824 0.53125 0.42377824 0.5625 0.42377824 0.59375 0.42377824;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 45 ".vt[0:44]"  -0.99999988 -1 0 -0.70710671 -1 0.70710671
		 0 -1 0.99999994 0.70710677 -1 0.70710677 1 -1 0 -0.99999988 1 0 -0.70710671 1 0.70710671
		 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0 1.20415044 -0.75894845 -7.7527744e-09
		 0.85146284 -0.75894845 0.85146284 -1.2168309e-08 -0.75894845 1.20415032 -0.85146272 -0.75894845 0.85146284
		 -1.20415032 -0.75894845 -7.7527744e-09 1.20415044 -0.18095623 -7.7527744e-09 0.85146284 -0.18095623 0.85146284
		 -1.2168309e-08 -0.18095623 1.2041502 -0.85146272 -0.18095623 0.85146284 -1.20415032 -0.18095623 -7.7527744e-09
		 1.20415044 0.35850307 -7.7527744e-09 0.85146284 0.35850307 0.85146284 -1.2168309e-08 0.35850307 1.20415032
		 -0.85146272 0.35850307 0.85146284 -1.20415032 0.35850307 -7.7527744e-09 1.20415044 0.84401655 -7.7527744e-09
		 0.85146284 0.84401655 0.85146284 -1.2168309e-08 0.84401655 1.2041502 -0.85146272 0.84401655 0.85146284
		 -1.20415032 0.84401655 -7.7527744e-09 -1.080408096 0.5666219 -3.9617793e-09 -0.76396376 0.5666219 0.76396388
		 -4.7926956e-09 0.5666219 1.080408096 0.76396388 0.5666219 0.76396388 1.080408216 0.5666219 -3.9617793e-09
		 -1.080408096 0.058397885 -3.9617793e-09 -0.76396376 0.058397885 0.76396388 -4.7926956e-09 0.058397885 1.080408096
		 0.76396388 0.058397885 0.76396388 1.080408216 0.058397885 -3.9617793e-09 -1.080408096 -0.44636881 -3.9617793e-09
		 -0.76396376 -0.44636881 0.76396388 -4.7926956e-09 -0.44636881 1.080408096 0.76396388 -0.44636881 0.76396388
		 1.080408216 -0.44636881 -3.9617793e-09;
	setAttr -s 76 ".ed[0:75]"  0 1 0 1 2 0 2 3 0 3 4 0 5 6 0 6 7 0 7 8 0
		 8 9 0 0 14 0 1 13 0 2 12 0 3 11 0 4 10 0 10 44 0 11 43 0 12 42 0 13 41 0 14 40 0
		 10 11 1 11 12 1 12 13 1 13 14 1 15 39 0 16 38 0 17 37 0 18 36 0 19 35 0 15 16 1 16 17 1
		 17 18 1 18 19 1 20 34 0 21 33 0 22 32 0 23 31 0 24 30 0 20 21 1 21 22 1 22 23 1 23 24 1
		 25 9 0 26 8 0 27 7 0 28 6 0 29 5 0 25 26 1 26 27 1 27 28 1 28 29 1 30 29 0 31 28 0
		 32 27 0 33 26 0 34 25 0 30 31 1 31 32 1 32 33 1 33 34 1 35 24 0 36 23 0 37 22 0 38 21 0
		 39 20 0 35 36 1 36 37 1 37 38 1 38 39 1 40 19 0 41 18 0 42 17 0 43 16 0 44 15 0 40 41 1
		 41 42 1 42 43 1 43 44 1;
	setAttr -s 32 -ch 128 ".fc[0:31]" -type "polyFaces" 
		f 4 0 9 21 -9
		mu 0 4 0 1 13 14
		f 4 1 10 20 -10
		mu 0 4 1 2 12 13
		f 4 2 11 19 -11
		mu 0 4 2 3 11 12
		f 4 3 12 18 -12
		mu 0 4 3 4 10 11
		f 4 75 71 27 -71
		mu 0 4 43 44 15 16
		f 4 74 70 28 -70
		mu 0 4 42 43 16 17
		f 4 73 69 29 -69
		mu 0 4 41 42 17 18
		f 4 72 68 30 -68
		mu 0 4 40 41 18 19
		f 4 66 62 36 -62
		mu 0 4 38 39 20 21
		f 4 65 61 37 -61
		mu 0 4 37 38 21 22
		f 4 64 60 38 -60
		mu 0 4 36 37 22 23
		f 4 63 59 39 -59
		mu 0 4 35 36 23 24
		f 4 57 53 45 -53
		mu 0 4 33 34 25 26
		f 4 56 52 46 -52
		mu 0 4 32 33 26 27
		f 4 55 51 47 -51
		mu 0 4 31 32 27 28
		f 4 54 50 48 -50
		mu 0 4 30 31 28 29
		f 4 -46 40 -8 -42
		mu 0 4 26 25 9 8
		f 4 -47 41 -7 -43
		mu 0 4 27 26 8 7
		f 4 -48 42 -6 -44
		mu 0 4 28 27 7 6
		f 4 -49 43 -5 -45
		mu 0 4 29 28 6 5
		f 4 -40 34 -55 -36
		mu 0 4 24 23 31 30
		f 4 -39 33 -56 -35
		mu 0 4 23 22 32 31
		f 4 -38 32 -57 -34
		mu 0 4 22 21 33 32
		f 4 -37 31 -58 -33
		mu 0 4 21 20 34 33
		f 4 -31 25 -64 -27
		mu 0 4 19 18 36 35
		f 4 -30 24 -65 -26
		mu 0 4 18 17 37 36
		f 4 -29 23 -66 -25
		mu 0 4 17 16 38 37
		f 4 -28 22 -67 -24
		mu 0 4 16 15 39 38
		f 4 -22 16 -73 -18
		mu 0 4 14 13 41 40
		f 4 -21 15 -74 -17
		mu 0 4 13 12 42 41
		f 4 -20 14 -75 -16
		mu 0 4 12 11 43 42
		f 4 -19 13 -76 -15
		mu 0 4 11 10 44 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder11";
	rename -uid "131E53D3-441B-DF4B-95B7-87B4A7E7344C";
	setAttr ".t" -type "double3" 8.695006715118911 6.4439221116556098 -2.1907817293964573 ;
	setAttr ".r" -type "double3" 0 -176.7366245911623 0 ;
	setAttr ".s" -type "double3" 2.428618273560355 6.2156743649956505 2.428618273560355 ;
createNode transform -n "transform2" -p "pCylinder11";
	rename -uid "05FA0716-430B-3D73-F6C7-93AAB83FE4C1";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape11" -p "transform2";
	rename -uid "4DBD71AC-49A1-07CB-F5A3-E9ABA5E4CA6C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:31]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.53125 0.50046992301940918 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 45 ".uvst[0].uvsp[0:44]" -type "float2" 0.46875 0.3125 0.5
		 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.59375 0.38768798 0.5625
		 0.38768798 0.53125 0.38768798 0.5 0.38768798 0.46875 0.38768798 0.59375 0.47791356
		 0.5625 0.47791356 0.53125 0.47791356 0.5 0.47791356 0.46875 0.47791356 0.59375 0.56212407
		 0.5625 0.56212407 0.53125 0.56212407 0.5 0.56212407 0.46875 0.56212407 0.59375 0.63791353
		 0.5625 0.63791353 0.53125 0.63791353 0.5 0.63791353 0.46875 0.63791353 0.46875 0.6000188
		 0.5 0.6000188 0.53125 0.6000188 0.5625 0.6000188 0.59375 0.6000188 0.46875 0.51159775
		 0.5 0.51159775 0.53125 0.51159775 0.5625 0.51159775 0.59375 0.51159775 0.46875 0.42377824
		 0.5 0.42377824 0.53125 0.42377824 0.5625 0.42377824 0.59375 0.42377824;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[2]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[3]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[6]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[7]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[8]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[11]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[12]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[13]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[16]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[17]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[18]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[21]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[22]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[23]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[26]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[27]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[28]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[31]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[32]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[33]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[36]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[37]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[38]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[41]" -type "float3" 0 0 -1.301634 ;
	setAttr ".pt[42]" -type "float3" 0 0 -2.0888288 ;
	setAttr ".pt[43]" -type "float3" 0 0 -1.301634 ;
	setAttr -s 45 ".vt[0:44]"  -0.99999988 -1 0 -0.70710671 -1 0.70710671
		 0 -1 0.99999994 0.70710677 -1 0.70710677 1 -1 0 -0.99999988 1 0 -0.70710671 1 0.70710671
		 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0 1.20415044 -0.75894845 -7.7527744e-09
		 0.85146284 -0.75894845 0.85146284 -1.2168309e-08 -0.75894845 1.20415032 -0.85146272 -0.75894845 0.85146284
		 -1.20415032 -0.75894845 -7.7527744e-09 1.20415044 -0.18095623 -7.7527744e-09 0.85146284 -0.18095623 0.85146284
		 -1.2168309e-08 -0.18095623 1.2041502 -0.85146272 -0.18095623 0.85146284 -1.20415032 -0.18095623 -7.7527744e-09
		 1.20415044 0.35850307 -7.7527744e-09 0.85146284 0.35850307 0.85146284 -1.2168309e-08 0.35850307 1.20415032
		 -0.85146272 0.35850307 0.85146284 -1.20415032 0.35850307 -7.7527744e-09 1.20415044 0.84401655 -7.7527744e-09
		 0.85146284 0.84401655 0.85146284 -1.2168309e-08 0.84401655 1.2041502 -0.85146272 0.84401655 0.85146284
		 -1.20415032 0.84401655 -7.7527744e-09 -1.080408096 0.5666219 -3.9617793e-09 -0.76396376 0.5666219 0.76396388
		 -4.7926956e-09 0.5666219 1.080408096 0.76396388 0.5666219 0.76396388 1.080408216 0.5666219 -3.9617793e-09
		 -1.080408096 0.058397885 -3.9617793e-09 -0.76396376 0.058397885 0.76396388 -4.7926956e-09 0.058397885 1.080408096
		 0.76396388 0.058397885 0.76396388 1.080408216 0.058397885 -3.9617793e-09 -1.080408096 -0.44636881 -3.9617793e-09
		 -0.76396376 -0.44636881 0.76396388 -4.7926956e-09 -0.44636881 1.080408096 0.76396388 -0.44636881 0.76396388
		 1.080408216 -0.44636881 -3.9617793e-09;
	setAttr -s 76 ".ed[0:75]"  0 1 0 1 2 0 2 3 0 3 4 0 5 6 0 6 7 0 7 8 0
		 8 9 0 0 14 0 1 13 0 2 12 0 3 11 0 4 10 0 10 44 0 11 43 0 12 42 0 13 41 0 14 40 0
		 10 11 1 11 12 1 12 13 1 13 14 1 15 39 0 16 38 0 17 37 0 18 36 0 19 35 0 15 16 1 16 17 1
		 17 18 1 18 19 1 20 34 0 21 33 0 22 32 0 23 31 0 24 30 0 20 21 1 21 22 1 22 23 1 23 24 1
		 25 9 0 26 8 0 27 7 0 28 6 0 29 5 0 25 26 1 26 27 1 27 28 1 28 29 1 30 29 0 31 28 0
		 32 27 0 33 26 0 34 25 0 30 31 1 31 32 1 32 33 1 33 34 1 35 24 0 36 23 0 37 22 0 38 21 0
		 39 20 0 35 36 1 36 37 1 37 38 1 38 39 1 40 19 0 41 18 0 42 17 0 43 16 0 44 15 0 40 41 1
		 41 42 1 42 43 1 43 44 1;
	setAttr -s 32 -ch 128 ".fc[0:31]" -type "polyFaces" 
		f 4 0 9 21 -9
		mu 0 4 0 1 13 14
		f 4 1 10 20 -10
		mu 0 4 1 2 12 13
		f 4 2 11 19 -11
		mu 0 4 2 3 11 12
		f 4 3 12 18 -12
		mu 0 4 3 4 10 11
		f 4 75 71 27 -71
		mu 0 4 43 44 15 16
		f 4 74 70 28 -70
		mu 0 4 42 43 16 17
		f 4 73 69 29 -69
		mu 0 4 41 42 17 18
		f 4 72 68 30 -68
		mu 0 4 40 41 18 19
		f 4 66 62 36 -62
		mu 0 4 38 39 20 21
		f 4 65 61 37 -61
		mu 0 4 37 38 21 22
		f 4 64 60 38 -60
		mu 0 4 36 37 22 23
		f 4 63 59 39 -59
		mu 0 4 35 36 23 24
		f 4 57 53 45 -53
		mu 0 4 33 34 25 26
		f 4 56 52 46 -52
		mu 0 4 32 33 26 27
		f 4 55 51 47 -51
		mu 0 4 31 32 27 28
		f 4 54 50 48 -50
		mu 0 4 30 31 28 29
		f 4 -46 40 -8 -42
		mu 0 4 26 25 9 8
		f 4 -47 41 -7 -43
		mu 0 4 27 26 8 7
		f 4 -48 42 -6 -44
		mu 0 4 28 27 7 6
		f 4 -49 43 -5 -45
		mu 0 4 29 28 6 5
		f 4 -40 34 -55 -36
		mu 0 4 24 23 31 30
		f 4 -39 33 -56 -35
		mu 0 4 23 22 32 31
		f 4 -38 32 -57 -34
		mu 0 4 22 21 33 32
		f 4 -37 31 -58 -33
		mu 0 4 21 20 34 33
		f 4 -31 25 -64 -27
		mu 0 4 19 18 36 35
		f 4 -30 24 -65 -26
		mu 0 4 18 17 37 36
		f 4 -29 23 -66 -25
		mu 0 4 17 16 38 37
		f 4 -28 22 -67 -24
		mu 0 4 16 15 39 38
		f 4 -22 16 -73 -18
		mu 0 4 14 13 41 40
		f 4 -21 15 -74 -17
		mu 0 4 13 12 42 41
		f 4 -20 14 -75 -16
		mu 0 4 12 11 43 42
		f 4 -19 13 -76 -15
		mu 0 4 11 10 44 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder12";
	rename -uid "3F36AE34-4D03-A3F6-5E9C-CDA57C6176D1";
	setAttr ".rp" -type "double3" 8.7679015363971153 6.4439221116556098 -0.39056556249791763 ;
	setAttr ".sp" -type "double3" 8.7679015363971153 6.4439221116556098 -0.39056556249791763 ;
createNode mesh -n "pCylinder12Shape" -p "pCylinder12";
	rename -uid "5E88F672-481A-85AF-B0D3-8D8DACA997A0";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.53125 0.50046992301940918 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 38 ".pt";
	setAttr ".pt[0]" -type "float3" 0.10017997 -0.25681174 -0.12260325 ;
	setAttr ".pt[4]" -type "float3" -0.10017991 -0.25681174 0.34017852 ;
	setAttr ".pt[5]" -type "float3" 0.10017997 0.25681174 -0.12260325 ;
	setAttr ".pt[9]" -type "float3" -0.10017991 0.25681174 0.34017852 ;
	setAttr ".pt[10]" -type "float3" -0.12063181 -0.19490686 0.38741741 ;
	setAttr ".pt[14]" -type "float3" 0.12063175 -0.19490686 -0.1698411 ;
	setAttr ".pt[15]" -type "float3" -0.12063181 -0.046471678 0.38741741 ;
	setAttr ".pt[19]" -type "float3" 0.12063175 -0.046471678 -0.1698411 ;
	setAttr ".pt[20]" -type "float3" -0.12063181 0.092067778 0.38741741 ;
	setAttr ".pt[24]" -type "float3" 0.12063175 0.092067778 -0.1698411 ;
	setAttr ".pt[25]" -type "float3" -0.12063181 0.21675335 0.38741741 ;
	setAttr ".pt[29]" -type "float3" 0.12063175 0.21675335 -0.1698411 ;
	setAttr ".pt[30]" -type "float3" 0.1082353 0.14551517 -0.14120856 ;
	setAttr ".pt[34]" -type "float3" -0.10823524 0.14551517 0.35878494 ;
	setAttr ".pt[35]" -type "float3" 0.1082353 0.014997265 -0.14120856 ;
	setAttr ".pt[39]" -type "float3" -0.10823524 0.014997265 0.35878494 ;
	setAttr ".pt[40]" -type "float3" 0.1082353 -0.11463276 -0.14120856 ;
	setAttr ".pt[44]" -type "float3" -0.10823524 -0.11463276 0.35878494 ;
	setAttr ".pt[45]" -type "float3" -0.10034263 -0.25681174 0.53704977 ;
	setAttr ".pt[49]" -type "float3" 0.10034269 -0.25681174 0.53704977 ;
	setAttr ".pt[50]" -type "float3" -0.10034263 0.25681174 0.53704977 ;
	setAttr ".pt[54]" -type "float3" 0.10034269 0.25681174 0.53704977 ;
	setAttr ".pt[55]" -type "float3" 0.12082767 -0.19490686 0.53704977 ;
	setAttr ".pt[59]" -type "float3" -0.12082767 -0.19490686 0.53704977 ;
	setAttr ".pt[60]" -type "float3" 0.12082767 -0.046471678 0.53704977 ;
	setAttr ".pt[64]" -type "float3" -0.12082767 -0.046471678 0.53704977 ;
	setAttr ".pt[65]" -type "float3" 0.12082767 0.092067778 0.53704977 ;
	setAttr ".pt[69]" -type "float3" -0.12082767 0.092067778 0.53704977 ;
	setAttr ".pt[70]" -type "float3" 0.12082767 0.21675335 0.53704977 ;
	setAttr ".pt[74]" -type "float3" -0.12082767 0.21675335 0.53704977 ;
	setAttr ".pt[75]" -type "float3" -0.10841107 0.14551517 0.53704977 ;
	setAttr ".pt[79]" -type "float3" 0.10841107 0.14551517 0.53704977 ;
	setAttr ".pt[80]" -type "float3" -0.10841107 0.014997265 0.53704977 ;
	setAttr ".pt[84]" -type "float3" 0.10841107 0.014997265 0.53704977 ;
	setAttr ".pt[85]" -type "float3" -0.10841107 -0.11463276 0.53704977 ;
	setAttr ".pt[89]" -type "float3" 0.10841107 -0.11463276 0.53704977 ;
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "A9113E66-4DAF-FA89-5A33-AC9467DB87D2";
	setAttr ".ics" -type "componentList" 30 "e[0:8]" "e[12:13]" "e[17]" "e[22]" "e[26]" "e[31]" "e[35]" "e[40]" "e[44]" "e[49]" "e[53]" "e[58]" "e[62]" "e[67]" "e[71]" "e[76:84]" "e[88:89]" "e[93]" "e[98]" "e[102]" "e[107]" "e[111]" "e[116]" "e[120]" "e[125]" "e[129]" "e[134]" "e[138]" "e[143]" "e[147]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 1;
	setAttr ".sv2" 48;
	setAttr ".d" 1;
createNode groupParts -n "groupParts1";
	rename -uid "3741C3F9-4621-FE0A-F7A7-30AC81122636";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:63]";
createNode polyUnite -n "polyUnite1";
	rename -uid "CF7AC851-4380-085F-0ADF-3885C4D3B2E2";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	rename -uid "1831FD0C-4A44-48CE-3945-1185E91882BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	rename -uid "DDBDA905-40BB-7748-611A-E4B5AFF87DCC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "1B8EB8A9-4FA4-95D1-6304-90B7B1E3CDD0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "16777AD3-43D8-C5D1-6FEB-F2B2F44A17B7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "3245C025-4058-569D-B00F-95B3255813B2";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 9 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupId3.id" "pCylinderShape9.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape9.iog.og[0].gco";
connectAttr "groupId4.id" "pCylinderShape9.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCylinderShape11.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape11.iog.og[0].gco";
connectAttr "groupId2.id" "pCylinderShape11.ciog.cog[0].cgid";
connectAttr "polyBridgeEdge1.out" "pCylinder12Shape.i";
connectAttr "groupId5.id" "pCylinder12Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder12Shape.iog.og[0].gco";
connectAttr "groupParts1.og" "polyBridgeEdge1.ip";
connectAttr "pCylinder12Shape.wm" "polyBridgeEdge1.mp";
connectAttr "polyUnite1.out" "groupParts1.ig";
connectAttr "groupId5.id" "groupParts1.gi";
connectAttr "pCylinderShape11.o" "polyUnite1.ip[0]";
connectAttr "pCylinderShape9.o" "polyUnite1.ip[1]";
connectAttr "pCylinderShape11.wm" "polyUnite1.im[0]";
connectAttr "pCylinderShape9.wm" "polyUnite1.im[1]";
connectAttr "pCylinderShape11.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape11.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape9.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape9.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder12Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
// End of CornerPillar1.ma
